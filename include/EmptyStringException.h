#include <stdexcept>
using namespace std;

class EmptyStringException : public runtime_error {
	public:
		EmptyStringException()
			: runtime_error("cannot encode empty string") {}
};
