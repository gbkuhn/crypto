#ifndef CRYPTOIMPL_H
#define CRYPTOIMPL_H

#include "Crypto.h"

/**
 *  CryptoImpl is an example implementation of the Crypto interface
 *  The pure virtual "encode" and "decode" functions of the Crypto
 *  interface are overridden by the CryptoImpl implementation.
 */
class CryptoImpl : public Crypto {
    public:
        CryptoImpl();
        virtual ~CryptoImpl();
        // Override the pure virtual functions from the Crypto class.
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // CRYPTOIMPL_H
