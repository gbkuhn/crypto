#ifndef KUHN_CRYPTO_H
#define KUHN_CRYPTO_H

#import "Crypto.h"

class Kuhn_crypto : public Crypto {
    public:
        Kuhn_crypto();
        virtual ~Kuhn_crypto();
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // KUHN_CRYPTO_H
