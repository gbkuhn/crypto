#ifndef CRYPTOGRAM_H
#define CRYPTOGRAM_H

#import "Crypto.h"

/**
 *  CryptoGram is an example implementation of the Crypto interface
 *  The pure virtual "encode" and "decode" functions of the Crypto
 *  interface are overridden by the CryptoGram implementation.
 */

class CryptoGram : public Crypto {
    public:
        CryptoGram();
        virtual ~CryptoGram();
        // Override the pure virtual functions from the Crypto class.
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // CRYPTOGRAM_H
