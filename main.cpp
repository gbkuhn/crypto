#include <iostream>
#include <memory>
#include "include/Crypto.h"
#include "include/CryptoImpl.h"
#include "include/CryptoChain.h"
//#include "include/ChristiesImpl.h" //christie
#include <stdexcept>
#include <fstream>
#include "include/Kuhn_crypto.h" // kuhn
#include "include/CryptoGram.h" // hutchinson

using namespace std;

int main() {
    cout << "Enter a word to be encrypted"<< endl;

  try {
    string input;
    getline (cin, input);
    //catches if no input
    if(input == ""){
           throw "Unable to encrypt without an input";
    }

    CryptoChain cc;

 //   auto_ptr<ChristiesImpl> c1(new ChristiesImpl);
//   auto_ptr<Kuhn_crypto> c2(new Kuhn_crypto);
    auto_ptr<CryptoGram> c3(new CryptoGram);

 //   cc.add(c1.get());
 //   cc.add(c2.get());
 k   cc.add(c3.get());

    string encoded = cc.encode(input);
    string decoded = cc.decode(encoded);

    cout << input << " -> " << encoded << " -> " << decoded;

  }
  catch(...)
  {
    cout<<"Unable to encrypt without an input"<<endl;
  }
  return 0;

}
