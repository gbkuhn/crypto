#include "../include/CryptoChain.h"
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

CryptoChain::CryptoChain() {

}

CryptoChain::~CryptoChain() {

}

void CryptoChain::add(Crypto *c) {
    cryptos.push_back(c);
}

string CryptoChain::encode(string s) {

    string result = s;

    // iterate forwards
    for (std::vector<Crypto*>::iterator it = cryptos.begin() ; it != cryptos.end(); ++it) {
        result = (*it)->encode(result);

        return result;
    }
}



string CryptoChain::decode(string s) {

    string result = s;


    // iterate backwards
    for (std::vector<Crypto*>::reverse_iterator it = cryptos.rbegin() ; it != cryptos.rend(); ++it) {
        result = (*it)->decode(result);

    }


    return result;

}
