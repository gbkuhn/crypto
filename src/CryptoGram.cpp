// Elliot Hutchinson
// Homework 4

#include "../include/CryptoGram.h"
#include "../include/EmptyStringException.h"
#include <iostream>
#include <string>
using namespace std;

CryptoGram::CryptoGram() {

}

CryptoGram::~CryptoGram() {

}
// Override the pure virtual functions from the Crypto class.

// Encodes by taking int representation of each character
// adds 2 and converts new int to character
string CryptoGram::encode(string s) {
	if (s.empty()) {
		throw EmptyStringException();
	}

	try {
		int ia = 0;
		char ai = '0';
		string temp = "";

		for (int i = 0; i < s.length(); i++) {
			ia = int(s[i]) + 2;
			ai = char(ia);
			temp.push_back(ai);
		}

		return temp;
	}
	catch (EmptyStringException &emptyStringException) {
		cout << "Exception occured: "
			 << emptyStringException.what() << endl;
	}
}

// Decodes by taking int representation of each character
// subtracts 2 and converts new int to character
string CryptoGram::decode(string s) {
	int ia = 0;
	char ai = '0';
	string temp = "";

	for (int i = 0; i < s.length(); i++) {
		ia = int(s[i]) - 2;
		ai = char(ia);
		temp.push_back(ai);
	}

    return temp;
}
